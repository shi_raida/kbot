/****************
 *** Includes ***
 ****************/

#include <Arduino.h>
#include <SimpleTimer.h>
#include <Servo.h>


/***************
 *** Defines ***
 ***************/

// Motors
#define MOT_LEFT1   11
#define MOT_LEFT2   12
#define MOT_RIGHT4  9
#define MOT_RIGHT3  10
#define ENC1_LEFT   2
#define ENC2_LEFT   3
#define ENC1_RIGHT  18
#define ENC2_RIGHT  19
#define LEFT        1
#define RIGHT       2
#define MAX_SPEED   255

// Serial
#define USB       Serial
#define BLUETOOTH Serial2

// Other
#define BTN_STOP  A11
#define READY_LED 47

// Commands
#define CMD_TERMINATOR        '\n'
#define MOVE_CMD              "move"
#define SWITCH_MODE_CMD       "switchMode"
#define SHARP_CMD             83
#define POSITION_CMD          80
#define MOVE_CMD_SIZE         4
#define SWITCH_MODE_CMD_SIZE  10

// Lidar
#define SERVO             5
#define SHARP             A7
#define MAX_DISTANCE      150
#define MIN_ANGLE         0
#define MAX_ANGLE         180
#define MIN_REAL_ANGLE    0
#define MAX_REAL_ANGLE    150
#define ANGLE_NB          30
#define AVERAGE_STEP      10
#define DELAY_DISTANCE    53
#define DELAY_RAZ_SERVO   450
#define CLOSE_DISTANCE    50
#define DISTANT_DISTANCE  100

// Space location
#define RIGHT_POS 1
#define RIGHT_NEG 2
#define LEFT_POS  3
#define LEFT_NEG  4

// Operating mode
#define MANUAL_MODE     1
#define AUTOMATIC_MODE  2
#define STOP_MODE       3

// K-Bot dimensions
#define R_WHEEL     3.65
#define R_K         10.25


/*****************
 *** Variables ***
 *****************/

SimpleTimer timer;
unsigned char mode = MANUAL_MODE;

// Lidar
Servo servo;
float angles[ANGLE_NB] = {0};
float distances[ANGLE_NB] = {100};
float averageDistances[ANGLE_NB / AVERAGE_STEP] = {0};
boolean distanceUp = true;
char distanceID = 0;
unsigned long servoTimer = 0;
int DELAY_SERVO = DELAY_RAZ_SERVO / (ANGLE_NB - 1);

// Space location
const double ELEM_R = 2*asin(R_WHEEL*PI / (R_K*720));
const double ELEM_X = R_WHEEL*PI*sin((PI-ELEM_R)/2)/360;
const double ELEM_Y = R_WHEEL*PI*cos((PI-ELEM_R)/2)/360;
volatile unsigned char tickID = 0;
volatile char tick[255] = {0};
double globalR = 0;
double globalX = 0;
double globalY = 0;
int timer_pos;


/***************************
 *** Functions prototype ***
 ***************************/

void sendPosition();
void calcNewPosition();
void readBluetooth();
void enc1Left();
void enc2Left();
void enc1Right();
void enc2Right();
void move(int, int);
void automaticMove();
void getDistanceBelonging(double*, double);
float readDistance();
void setSpeed(int, int);
void initDistances();
unsigned char idToAngle(unsigned char);


/*****************
 *** Functions ***
 *****************/

void setup() {
  pinMode(MOT_LEFT1, OUTPUT);
  pinMode(MOT_LEFT2, OUTPUT);
  pinMode(MOT_RIGHT3, OUTPUT);
  pinMode(MOT_RIGHT4, OUTPUT);
  pinMode(READY_LED, OUTPUT);
  pinMode(BTN_STOP, INPUT_PULLUP);
  pinMode(SHARP, INPUT);

  USB.begin(19200);
  BLUETOOTH.begin(9600);
  USB.println("---------- Start ----------");
  digitalWrite(READY_LED, HIGH);

  pinMode(ENC1_LEFT, INPUT);
  pinMode(ENC2_LEFT, INPUT);
  pinMode(ENC1_RIGHT, INPUT);
  pinMode(ENC2_RIGHT, INPUT);
  attachInterrupt(digitalPinToInterrupt(ENC1_LEFT), enc1Left, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ENC2_LEFT), enc2Left, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ENC1_RIGHT), enc1Right, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ENC2_RIGHT), enc2Right, CHANGE);

  timer_pos = timer.setInterval(100, sendPosition);
}


void loop() {
  if (mode != STOP_MODE) {
    readBluetooth();

    if (digitalRead(BTN_STOP) == LOW) {
      mode = STOP_MODE;
      setSpeed(0, RIGHT);
      setSpeed(0, LEFT);
      timer.disable(timer_pos);
      digitalWrite(READY_LED, LOW);
      servo.detach();
      USB.println("---------- Stop ----------");
    }

    timer.run();

    if (mode == AUTOMATIC_MODE && (millis() - servoTimer) > DELAY_SERVO)
      automaticMove();
  }
}


#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise"
void sendPosition() {
  calcNewPosition();
  
  int x = (int)globalX;
  byte x0 = x < 0 ? 1 : 0;
  byte x1 = x & 0b11111111;
  byte x2 = x >> 8;

  int y = (int)globalY;
  byte y0 = y < 0 ? 1 : 0;
  byte y1 = y & 0b11111111;
  byte y2 = y >> 8;

  int r = (int)(globalR*57.29577951308232); // 180/pi
  byte r0 = r < 0 ? 1 : 0;
  byte r1 = r & 0b11111111;
  byte r2 = r >> 8;

  BLUETOOTH.write(POSITION_CMD);
  BLUETOOTH.write(x0);
  BLUETOOTH.write(x1);
  BLUETOOTH.write(x2);
  BLUETOOTH.write(y0);
  BLUETOOTH.write(y1);
  BLUETOOTH.write(y2);
  BLUETOOTH.write(r0);
  BLUETOOTH.write(r1);
  BLUETOOTH.write(r2);
}
#pragma clang diagnostic pop


void calcNewPosition() {
  for (int i = 0; i < tickID; i++) {
    double cosine = cos(globalR);
    double sinus = sin(globalR);
    switch (tick[i]) {
      case LEFT_POS:
        globalX += ELEM_X * cosine - ELEM_Y * sinus;
        globalY += ELEM_X * sinus + ELEM_Y * cosine;
        globalR -= ELEM_R;
        break;
      case LEFT_NEG:
        globalX -= ELEM_X * cosine - ELEM_Y * sinus;
        globalY -= ELEM_X * sinus + ELEM_Y * cosine;
        globalR += ELEM_R;
        break;
      case RIGHT_POS:
        globalX += ELEM_X * cosine - ELEM_Y * sinus;
        globalY += ELEM_X * sinus + ELEM_Y * cosine;
        globalR += ELEM_R;
        break;
      case RIGHT_NEG:
        globalX -= ELEM_X*cosine - ELEM_Y*sinus;
        globalY -= ELEM_X*sinus + ELEM_Y*cosine;
        globalR -= ELEM_R;
        break;
    }
  }
  
  tickID = 0;
}


void readBluetooth() {
  if (BLUETOOTH.available()) {
    String cmd = BLUETOOTH.readStringUntil(CMD_TERMINATOR);
    if (cmd.substring(0, MOVE_CMD_SIZE) == MOVE_CMD) {
      
      int angle = (int)cmd.substring(MOVE_CMD_SIZE+1, cmd.indexOf(",")+1).toInt();
      int s = (int)cmd.substring(cmd.indexOf(",")+1, cmd.indexOf(")")+1).toInt();
      move(angle, s);
      
    } else if (cmd.substring(0, SWITCH_MODE_CMD_SIZE) == SWITCH_MODE_CMD) {
      
      mode = cmd.substring(SWITCH_MODE_CMD_SIZE, SWITCH_MODE_CMD_SIZE+1).toInt() == 1 ? AUTOMATIC_MODE : MANUAL_MODE;
      move(0, 0);
      if (mode == AUTOMATIC_MODE) {
        servo.attach(SERVO);
        initDistances();
      } else servo.detach();
    }
  }
}


void enc1Left() {
  if (digitalRead(ENC1_LEFT) == HIGH) {
    if (digitalRead(ENC2_LEFT) == HIGH) {
      tick[tickID] = LEFT_POS;
    } else {
      tick[tickID] = LEFT_NEG;
    }
  } else {
    if (digitalRead(ENC2_LEFT) == HIGH) {
      tick[tickID] = LEFT_NEG;
    } else {
      tick[tickID] = LEFT_POS;
    }
  }
  tickID++;
}


void enc2Left() {
  if (digitalRead(ENC2_LEFT) == HIGH) {
    if (digitalRead(ENC1_LEFT) == HIGH) {
      tick[tickID] = LEFT_NEG;
    } else {
      tick[tickID] = LEFT_POS;
    }
  } else {
    if (digitalRead(ENC1_LEFT) == HIGH) {
      tick[tickID] = LEFT_POS;
    } else {
      tick[tickID] = LEFT_NEG;
    }
  }
  tickID++;
}


void enc1Right() {
  if (digitalRead(ENC1_RIGHT) == HIGH) {
    if (digitalRead(ENC2_RIGHT) == HIGH) {
      tick[tickID] = RIGHT_POS;
    } else {
      tick[tickID] = RIGHT_NEG;
    }
  } else {
    if (digitalRead(ENC2_RIGHT) == HIGH) {
      tick[tickID] = RIGHT_NEG;
    } else {
      tick[tickID] = RIGHT_POS;
    }
  }
  tickID++;
}


void enc2Right() {
  if (digitalRead(ENC2_RIGHT) == HIGH) {
    if (digitalRead(ENC1_RIGHT) == HIGH) {
      tick[tickID] = RIGHT_NEG;
    } else {
      tick[tickID] = RIGHT_POS;
    }
  } else {
    if (digitalRead(ENC1_RIGHT) == HIGH) {
      tick[tickID] = RIGHT_POS;
    } else {
      tick[tickID] = RIGHT_NEG;
    }
  }
  tickID++;
}


void move(int angle, int s) {
  double left = MAX_SPEED * s / 100.;
  double right = MAX_SPEED * s / 100.;

  // *0.022222222 = /45.
  if (angle >=0 && angle < 90) {
    left *= -angle * 0.022222222 + 1;
    right *= 1;
  } else if (angle >= 90 && angle < 180) {
    left *= -1;
    right *= (90 - angle) * 0.022222222 + 1;
  } else if (angle >= 180 && angle < 270) {
    left *= (angle - 180) * 0.022222222 - 1;
    right *= -1;
  } else if (angle >= 270 && angle < 360) {
    left *= 1;
    right *= (angle - 270) * 0.022222222 - 1;
  } else {
    left = 0;
    right = 0;
  }
  
  setSpeed((int)left, LEFT);
  setSpeed((int)right, RIGHT);
}


#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise"
void automaticMove() {
  averageDistances[distanceID / AVERAGE_STEP] -= distances[distanceID] / AVERAGE_STEP;
  distances[distanceID] = readDistance();
  averageDistances[distanceID / AVERAGE_STEP] += distances[distanceID] / AVERAGE_STEP;

  int d1 = (int)distances[distanceID] & 0b11111111;
  int d2 = (int)distances[distanceID] >> 8;

  BLUETOOTH.write(SHARP_CMD);
  BLUETOOTH.write((byte)(angles[distanceID] * 180 / PI));
  BLUETOOTH.write(d1);
  BLUETOOTH.write(d2);
  
  distanceID += distanceUp ? 1 : -1;
  if (distanceID >= ANGLE_NB) {
    distanceID -= 2;
    distanceUp = false;
  }
  if (distanceID < 0) {
    distanceID += 2;
    distanceUp = true;
  }

  angles[distanceID] = (float)(idToAngle(distanceID) * PI / 180);
  servo.write((int)map(idToAngle(distanceID), MIN_ANGLE, MAX_ANGLE, MIN_REAL_ANGLE, MAX_REAL_ANGLE));
  servoTimer = millis();

  // Distances: [close, distant]
  double rightDistance[2];
  double centerDistance[2];
  double leftDistance[2];
  // Speeds: [backward, stop, forward]
  double rightSpeed[3] = {0};
  double leftSpeed[3] = {0};
  
  getDistanceBelonging(rightDistance, averageDistances[0]);
  getDistanceBelonging(centerDistance, averageDistances[1]);
  getDistanceBelonging(leftDistance, averageDistances[2]);

  // Case 1 -- Right: distant - Center: distant - Left: distant
  rightSpeed[3] = max(rightSpeed[3], min(min(rightDistance[1], centerDistance[1]), leftDistance[1]));
  leftSpeed[3] = max(leftSpeed[3], min(min(rightDistance[1], centerDistance[1]), leftDistance[1]));

  // Case 2 -- Right: distant - Center: distant - Left: close
  rightSpeed[2] = max(rightSpeed[2], min(min(rightDistance[1], centerDistance[1]), leftDistance[0]));
  leftSpeed[3] = max(leftSpeed[3], min(min(rightDistance[1], centerDistance[1]), leftDistance[0]));

  // Case 3 -- Right: close - Center: distant - Left: distant
  rightSpeed[3] = max(rightSpeed[3], min(min(rightDistance[0], centerDistance[1]), leftDistance[1]));
  leftSpeed[2] = max(leftSpeed[2], min(min(rightDistance[0], centerDistance[1]), leftDistance[1]));

  // Case 4 -- Right: distant - Center: close - Left: distant
  rightSpeed[1] = max(rightSpeed[1], min(min(rightDistance[1], centerDistance[0]), leftDistance[1]));
  leftSpeed[3] = max(leftSpeed[3], min(min(rightDistance[1], centerDistance[0]), leftDistance[1]));

  // Case 5 -- Right: distant - Center: close - Left: close
  rightSpeed[1] = max(rightSpeed[1], min(min(rightDistance[1], centerDistance[0]), leftDistance[0]));
  leftSpeed[3] = max(leftSpeed[3], min(min(rightDistance[1], centerDistance[0]), leftDistance[0]));

  // Case 6 -- Right: close - Center: close - Left: distant
  rightSpeed[3] = max(rightSpeed[3], min(min(rightDistance[0], centerDistance[0]), leftDistance[1]));
  leftSpeed[1] = max(leftSpeed[1], min(min(rightDistance[0], centerDistance[0]), leftDistance[1]));

  // Case 7 -- Right: close - Center: distant - Left: close
  rightSpeed[3] = max(rightSpeed[3], min(min(rightDistance[0], centerDistance[1]), leftDistance[0]));
  leftSpeed[3] = max(leftSpeed[3], min(min(rightDistance[0], centerDistance[1]), leftDistance[0]));

  // Case 8 -- Right: close - Center: close - Left: close
  rightSpeed[1] = max(rightSpeed[1], min(min(rightDistance[0], centerDistance[0]), leftDistance[0]));
  leftSpeed[3] = max(leftSpeed[3], min(min(rightDistance[0], centerDistance[0]), leftDistance[0]));

  setSpeed(MAX_SPEED * (rightSpeed[3] - rightSpeed[1]) / (rightSpeed[1] + rightSpeed[2] + rightSpeed[3]), RIGHT);
  setSpeed(MAX_SPEED * (leftSpeed[3] - leftSpeed[1]) / (leftSpeed[1] + leftSpeed[2] + leftSpeed[3]), LEFT);
}
#pragma clang diagnostic pop


/**
 * return [close, distant]
 */
void getDistanceBelonging(double distanceBelonging[], double distance) {
  if (distance < CLOSE_DISTANCE) {  // Close
    distanceBelonging[0] = 1;
    distanceBelonging[1] = 0;
  } else if (distance > DISTANT_DISTANCE) {  // Distant
    distanceBelonging[0] = 0;
    distanceBelonging[1] = 1;
  } else {
    distanceBelonging[0] = (CLOSE_DISTANCE - distance) / (DISTANT_DISTANCE - CLOSE_DISTANCE) + 1;
    distanceBelonging[1] = (distance - CLOSE_DISTANCE) / (DISTANT_DISTANCE - CLOSE_DISTANCE);
  }
}


#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-use-auto"
float readDistance() {
  float value = (float)(-62696873887607332864. / (sqrt(5053828433.)*sqrt(6291678592338342249908730585.-9645788653860612946460672.*(float)analogRead(SHARP))-5584999532560152461.));
  delay(DELAY_DISTANCE);
  if (value >= 0 && value < MAX_DISTANCE) return value;
  else                                    return 4*MAX_DISTANCE;
}
#pragma clang diagnostic pop


void setSpeed(int s, int mot) {
  if (mot == RIGHT) {
    if (s > 0) {
      analogWrite(MOT_RIGHT3, 0);
      analogWrite(MOT_RIGHT4, s);
    } else {
      analogWrite(MOT_RIGHT3, -s);
      analogWrite(MOT_RIGHT4, 0);
    }
  } else if (mot == LEFT) {
    if (s > 0) {
      analogWrite(MOT_LEFT1, 0);
      analogWrite(MOT_LEFT2, s);
    } else {
      analogWrite(MOT_LEFT1, -s);
      analogWrite(MOT_LEFT2, 0);
    }
  }
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wfor-loop-analysis"
#pragma ide diagnostic ignored "hicpp-signed-bitwise"
void initDistances() {
  servo.write(MIN_REAL_ANGLE);
  delay(DELAY_RAZ_SERVO);

  for (int i = 0; i < ANGLE_NB; i++) {
    angles[i] = (float)(idToAngle(i) * PI / 180);
    servo.write((int)map(idToAngle(i), MIN_ANGLE, MAX_ANGLE, MIN_REAL_ANGLE, MAX_REAL_ANGLE));
    delay(DELAY_SERVO);
    distances[i] = readDistance();
    averageDistances[i / AVERAGE_STEP] += distances[i] / AVERAGE_STEP;

    byte d1 = (int)distances[i] & 0b11111111;
    byte d2 = (int)distances[i] >> 8;
    
    BLUETOOTH.write(SHARP_CMD);
    BLUETOOTH.write((byte)(angles[i] * 180 / PI));
    BLUETOOTH.write(d1);
    BLUETOOTH.write(d2);
  }

  distanceID = ANGLE_NB-1;
}
#pragma clang diagnostic pop


unsigned char idToAngle(unsigned char i) {
  return MIN_ANGLE + i * (MAX_ANGLE - MIN_ANGLE) / (ANGLE_NB - 1);
}
