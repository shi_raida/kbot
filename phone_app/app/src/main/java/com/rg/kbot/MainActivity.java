package com.rg.kbot;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import io.github.controlwear.virtual.joystick.android.JoystickView;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "KBOT_APP";

    private static final int PERMISSIONS_REQUEST_BLUETOOTH = 1;
    private static final int PERMISSIONS_REQUEST_BLUETOOTH_ADMIN = 2;

    private static final int REQUEST_ENABLE_BLUETOOTH = 1;

    private static final char CMD_TERMINATOR = '\n';
    private static final String MOVE_CMD = "move";
    private static final String SWITCH_MODE_CMD = "switchMode";

    private static final int ANGLE_NB = 30;
    private static final double MAX_DISTANCE = 150;

    private BluetoothAdapter mBluetoothAdapter;
    private GraphView mSharpDataView;
    private ArrayList<DataPoint> mSharpData;
    private int mSharpIndex;
    private boolean mSharpUp;

    private TextView mTextViewAngle;
    private TextView mTextViewStrength;

    private Switch mSwitchMode;

    private Button mConnectButton;
    private Spinner mBluetoothDevicesSpinner;
    private List<BluetoothDevice> mBluetoothDevices;

    private Handler mmHandler;
    private BluetoothConnectionThread mConnectionThread;
    private BluetoothConnectedThread mConnectedThread;

    private int currentX;
    private int currentY;
    private int currentR;


    private interface MessageConstants {
        int MESSAGE_WRITE = 1;
        int MESSAGE_TOAST = 2;
        int MESSAGE_CONNECTED = 3;
        int MESSAGE_DISCONNECTED = 4;
        int MESSAGE_SHARP_CMD = 5;
        int MESSAGE_POSITION_CMD = 6;

        String TOAST_TEXT = "ToastId";
    }

    private interface Command {
        int SHARP = 83;
        int POSITION = 80;
    }

    private interface CommandSize {
        int SHARP = 4;
        int POSITION = 10;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissions();

        createBluetooth();
        run();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_BLUETOOTH: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Bluetooth permission granted after request");
                } else {
                    Log.w(TAG, "Bluetooth permission not granted after request");
                }
                return;
            }
            case PERMISSIONS_REQUEST_BLUETOOTH_ADMIN: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Bluetooth Admin permission granted after request");
                } else {
                    Log.w(TAG, "Bluetooth Admin permission not granted after request");
                }
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == RESULT_CANCELED) {
                Log.w(TAG, "Bluetooth not activated after request");
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("L'application a besoin du bluetooth pour ce connecter à votre "+
                        "robot. Merci de l'activer.")
                        .setTitle("Bluetooth non activé");
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                Log.v(TAG, "Bluetooth activated after request");
                run();

            }
        }
    }


    private void checkPermissions() {
        Log.v(TAG, "Checking permissions...");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH)
                != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "Bluetooth permission not granted");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.BLUETOOTH},
                    PERMISSIONS_REQUEST_BLUETOOTH);
        } else {
            Log.v(TAG, "Bluetooth permission granted");
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN)
                != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "Bluetooth Admin permission not granted");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.BLUETOOTH_ADMIN},
                    PERMISSIONS_REQUEST_BLUETOOTH_ADMIN);
        } else {
            Log.v(TAG, "Bluetooth Admin permission granted");
        }
        Log.v(TAG, "Permissions checked !");
    }


    private void createBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Bluetooth not supported");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Le Bluetooth n'est pas pris en charge par votre machine. " +
                    "Vous ne pouvez donc pas utiliser cette application.")
                    .setTitle("Bluetooth non pris en charge");
            AlertDialog dialog = builder.create();
            dialog.show();
        } else if (!mBluetoothAdapter.isEnabled()) {
            Log.w(TAG, "Bluetooth not activated");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLUETOOTH);
        } else {
            Log.v(TAG, "Bluetooth activated");
        }

    }


    private void getBluetoothDevices() {
        Log.v(TAG, "Searching Bluetooth devices...");
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        mBluetoothDevicesSpinner = findViewById(R.id.bluetoothDevices);
        mBluetoothDevices = new ArrayList<>();
        List<String> devicesName = new ArrayList<>();

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                mBluetoothDevices.add(device);
                devicesName.add(device.getName());
                Log.v(TAG, "Find " + device.getName());
            }
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, devicesName);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBluetoothDevicesSpinner.setAdapter(dataAdapter);
        Log.v(TAG, "Searching done !");
    }


    @SuppressLint("SetTextI18n")
    private void connectButton() {
        if (mConnectButton == null) {
            Log.v(TAG, "Getting button");
            mConnectButton = findViewById(R.id.connectButton);
        }

        Log.v(TAG, "Connecting button");
        mConnectButton.setText("Connect");
        mConnectButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String deviceName = String.valueOf(mBluetoothDevicesSpinner.getSelectedItem());
                Log.v(TAG, "Selected device: " + deviceName);
                BluetoothDevice device = null;
                for (int i = 0; i < mBluetoothDevices.size(); i++) {
                    if (mBluetoothDevices.get(i).getName().equals(deviceName)) {
                        device = mBluetoothDevices.get(i);
                        break;
                    }
                }
                if (device != null) {
                    Log.v(TAG, "Bluetooth connexion to " + device.getName() +  "...");
                    mConnectionThread = new BluetoothConnectionThread(device);
                    mConnectionThread.start();
                }
            }

        });
    }


    @SuppressLint("SetTextI18n")
    private void disconnectButton() {
        if (mConnectButton == null) {
            Log.v(TAG, "Getting button");
            mConnectButton = findViewById(R.id.connectButton);
        }

        Log.v(TAG, "Disconnecting button");
        mConnectButton.setText("Disconnect");
        mConnectButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mConnectedThread.cancel();
            }

        });
    }


    @SuppressLint("HandlerLeak")
    private void createHandler() {
        mmHandler = new Handler() {

            @Override
            public void handleMessage(@NonNull Message msg) {
                switch (msg.what) {
                    case MessageConstants.MESSAGE_CONNECTED: {
                        disconnectButton();
                        Message toastMsg =
                                mmHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
                        Bundle bundle = new Bundle();
                        bundle.putString(MessageConstants.TOAST_TEXT,
                                "Connected to " + msg.obj);
                        toastMsg.setData(bundle);
                        mmHandler.sendMessage(toastMsg);
                        return;
                    }
                    case MessageConstants.MESSAGE_DISCONNECTED: {
                        connectButton();
                        Message toastMsg =
                                mmHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
                        Bundle bundle = new Bundle();
                        bundle.putString(MessageConstants.TOAST_TEXT,
                                "Disconnected from " + msg.obj);
                        toastMsg.setData(bundle);
                        mmHandler.sendMessage(toastMsg);
                        return;
                    }
                    case MessageConstants.MESSAGE_TOAST: {
                        String txt = msg.getData().getString(MessageConstants.TOAST_TEXT);
                        Toast.makeText(MainActivity.this, txt, Toast.LENGTH_SHORT)
                                .show();
                        return;
                    }
                    case MessageConstants.MESSAGE_SHARP_CMD: {
                        Log.v(TAG, "Sharp command: " + Arrays.toString((Object[])msg.obj));

                        int angle = (byte)((Object[])msg.obj)[1];
                        int d1 = (byte)((Object[])msg.obj)[2];
                        int d2 = (byte)((Object[])msg.obj)[3];

                        if (angle < 0)  angle += 0b100000000;
                        if (d1 < 0)     d1    += 0b100000000;
                        if (d2 < 0)     d2    += 0b100000000;
                        int distance = d2 << 8 | d1;
                        Log.d(TAG, String.valueOf(distance));

                        updateSharpData(angle, distance);
                        return;
                    }
                    case MessageConstants.MESSAGE_WRITE: {
                        Log.v(TAG, "Send data: " + Arrays.toString((byte[])msg.obj));
                        return;
                    }
                    case MessageConstants.MESSAGE_POSITION_CMD:
                        Log.v(TAG, "Position command: " + Arrays.toString((Object[])msg.obj));

                        int x0 = (byte)((Object[])msg.obj)[1];
                        int x1 = (byte)((Object[])msg.obj)[2];
                        int x2 = (byte)((Object[])msg.obj)[3];
                        int y0 = (byte)((Object[])msg.obj)[4];
                        int y1 = (byte)((Object[])msg.obj)[5];
                        int y2 = (byte)((Object[])msg.obj)[6];
                        int r0 = (byte)((Object[])msg.obj)[7];
                        int r1 = (byte)((Object[])msg.obj)[8];
                        int r2 = (byte)((Object[])msg.obj)[9];

                        if (x1 < 0) x1 += 0b100000000;
                        if (x2 < 0) x2 += 0b100000000;
                        if (y1 < 0) y1 += 0b100000000;
                        if (y2 < 0) y2 += 0b100000000;
                        if (r1 < 0) r1 += 0b100000000;
                        if (r2 < 0) r2 += 0b100000000;

                        int x =  x2 << 8 | x1;
                        int y =  y2 << 8 | y1;
                        int r =  r2 << 8 | r1;

                        if (x0 == 1) x -= 0b10000000000000000;
                        if (y0 == 1) y -= 0b10000000000000000;
                        if (r0 == 1) r -= 0b10000000000000000;

                        updatePositionData(x, y, r);
                }
            }

        };
    }


    private void createJoystick() {
        Log.v(TAG, "Joystick creation");
        JoystickView mJoystick = findViewById(R.id.joystickView);
        mJoystick.setOnMoveListener(new JoystickView.OnMoveListener() {

            @Override
            public void onMove(int angle, int strength) {
                angle -= 90;
                if (angle < 0) angle += 360;

                updateUI(angle, strength);

                if (mConnectedThread != null) {
                    if (mConnectedThread.isConnected() && !mSwitchMode.isChecked()) {
                        String cmd = MOVE_CMD + "(" + angle + "," + strength + ")" + CMD_TERMINATOR;
                        mConnectedThread.write(cmd.getBytes());
                    }
                }
            }

        });
    }


    private void createSwitchMode() {
        Log.v(TAG, "Swicth Mode creation");
        mSwitchMode = findViewById(R.id.switch_mode);
        if (mSwitchMode.isChecked()) {
            mSwitchMode.setText(mSwitchMode.getTextOn());
        } else {
            mSwitchMode.setText(mSwitchMode.getTextOff());
        }
        mSwitchMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {  // Automatic mode
                    mSwitchMode.setText(mSwitchMode.getTextOn());
                    // Reset sharp representation
                    mSharpIndex = 0;
                    mSharpUp = true;
                } else {
                    mSwitchMode.setText(mSwitchMode.getTextOff());
                }

                if (mConnectedThread != null) {
                    if (mConnectedThread.isConnected()) {
                        String cmd;
                        if (b) {
                            cmd = SWITCH_MODE_CMD + "1" + CMD_TERMINATOR;
                        } else {
                            cmd = SWITCH_MODE_CMD + "0" + CMD_TERMINATOR;
                        }
                        mConnectedThread.write(cmd.getBytes());
                    }
                }
            }

        });
    }


    private void createSharpData() {
        Log.v(TAG, "Sharp Data creation");
        mSharpDataView = findViewById(R.id.sharp_data);
        mSharpDataView.getViewport().setMinX(-MAX_DISTANCE);
        mSharpDataView.getViewport().setMaxX(MAX_DISTANCE);
        mSharpDataView.getViewport().setMinY(0);
        mSharpDataView.getViewport().setMaxY(MAX_DISTANCE);
        mSharpDataView.getViewport().setYAxisBoundsManual(true);
        mSharpDataView.getViewport().setXAxisBoundsManual(true);

        mSharpData = new ArrayList<>();

        for(int i = 0; i < ANGLE_NB; i++) {
            mSharpData.add(new DataPoint(0, 0));
        }
        updateSharpSeries();
    }


    private void updateSharpData(int angle, int distance) {
        double x = distance * cos(angle * PI / 180);
        double y = distance * sin(angle * PI / 180);
        mSharpData.set(mSharpIndex, new DataPoint(x, y));

        mSharpIndex += mSharpUp ? 1 : -1;
        if (mSharpIndex >= ANGLE_NB) {
            mSharpIndex -= 2;
            mSharpUp = false;
        }
        if (mSharpIndex < 0) {
            mSharpIndex += 2;
            mSharpUp = true;
        }

        updateSharpSeries();
    }


    private void updateSharpSeries() {
        mSharpDataView.removeAllSeries();
        PointsGraphSeries<DataPoint> series = new PointsGraphSeries<>();
        boolean[] check = new boolean[mSharpData.size()];
        for (int i = 0; i < mSharpData.size(); i++) {
            double minX = Double.MAX_VALUE;
            int minID = -1;
            for (int j = 0; j < mSharpData.size(); j++) {
                if (check[j]) continue;
                if (mSharpData.get(j).getX() < minX) {
                    minX = mSharpData.get(j).getX();
                    minID = j;
                }
            }
            series.appendData(mSharpData.get(minID), false, mSharpData.size());
            check[minID] = true;
        }
        mSharpDataView.addSeries(series);
    }


    private void updatePositionData(int x, int y, int r) {
        int deltaX = x - currentX;
        int deltaY = y - currentY;
        int deltaR = r - currentR;

        double currentAngle = currentR * PI / 180;
        double angle = deltaR * PI / 180;

        for (int i = 0; i < mSharpData.size(); i++) {
            DataPoint data = mSharpData.get(i);

            double tmpX = data.getX() - deltaX * sin(currentAngle) - deltaY * cos(currentAngle);
            double tmpY = data.getY() - deltaX * cos(currentAngle) - deltaY * sin(currentAngle);
            double newX = cos(angle) * tmpX + sin(angle) * tmpY;
            double newY = cos(angle) * tmpY - sin(angle) * tmpX;

            mSharpData.set(i, new DataPoint(newX, newY));
        }
        updateSharpSeries();

        currentX = x;
        currentY = y;
        currentR = r;

        StringBuilder position = new StringBuilder("(");
        position.append(x);
        position.append(", ");
        position.append(y);
        position.append(", ");
        position.append(r);
        position.append(")");
        TextView positionView = findViewById(R.id.textView_position);
        positionView.setText(position);
    }


    private void run() {
        if (mBluetoothAdapter != null) {
            if (mBluetoothAdapter.isEnabled()) {
                currentX = 0;
                currentY = 0;
                currentR = 0;
                createJoystick();
                createHandler();
                getBluetoothDevices();
                connectButton();
                createSwitchMode();
                createSharpData();
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private void updateUI(int angle, int strength) {
        if (mTextViewAngle == null) {
            Log.v(TAG, "Getting Angle TextView");
            mTextViewAngle = findViewById(R.id.textView_angle);
        }
        if (mTextViewStrength == null) {
            Log.v(TAG, "Getting Strength TextView");
            mTextViewStrength = findViewById(R.id.textView_strength);
        }

        Log.v(TAG, "Updating Angle and Strength text: (" + angle + ", " + strength + ")");
        mTextViewAngle.setText(angle + "°");
        mTextViewStrength.setText(strength + "%");
    }




    /*****************************
     *          THREADS          *
     *****************************/

    private class BluetoothConnectionThread extends Thread {
        private final BluetoothSocket mSocket;
        private final BluetoothDevice mDevice;

        @SuppressWarnings("JavaReflectionMemberAccess")
        BluetoothConnectionThread(BluetoothDevice device) {
            Log.v(TAG, "BluetoothConnectionThread creation...");
            BluetoothSocket tmp = null;
            mDevice = device;

            try {
                if (mDevice != null) {
                    Method m = mDevice.getClass().getMethod("createRfcommSocket", int.class);
                    tmp = (BluetoothSocket) m.invoke(device, 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mSocket = tmp;
            Log.v(TAG, "BluetoothConnectionThread created !");
        }

        public void run() {
            Log.v(TAG, "BluetoothConnectionThread running...");
            mBluetoothAdapter.cancelDiscovery();

            try {
                Log.v(TAG, "Socket connection...");
                mSocket.connect();
            } catch (IOException connectException) {
                Log.e(TAG, "Could not connect to the client socket", connectException);
                Message writeErrorMsg =
                        mmHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                bundle.putString(MessageConstants.TOAST_TEXT,
                        "Could not connect to the " + mDevice.getName() + " device");
                writeErrorMsg.setData(bundle);
                mmHandler.sendMessage(writeErrorMsg);
                try {
                    Log.v(TAG, "Socket closure...");
                    mSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }
            Log.v(TAG, "Bluetooth connected !");
            mmHandler.obtainMessage(
                    MessageConstants.MESSAGE_CONNECTED, mDevice.getName()
            ).sendToTarget();
            mConnectedThread = new BluetoothConnectedThread(mSocket);
            mConnectedThread.start();
        }

//        public void cancel() {
//            try {
//                Log.v(TAG, "Closing client socket with BluetoothConnectionThread");
//                mmHandler.obtainMessage(
//                        MessageConstants.MESSAGE_CONNECTED, mDevice.getName()
//                ).sendToTarget();
//                mSocket.close();
//            } catch (IOException e) {
//                Log.e(TAG, "Could not close the client socket", e);
//            }
//        }
    }




    private class BluetoothConnectedThread extends Thread {
        private final BluetoothSocket mSocket;
        private final InputStream mInStream;
        private final OutputStream mOutStream;
        private byte[] mBuffer;

        BluetoothConnectedThread(BluetoothSocket socket) {
            Log.v(TAG, "BluetoothConnectedThread creation...");
            mSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                Log.v(TAG, "Getting input stream");
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }
            try {
                Log.v(TAG, "Getting output stream");
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            mInStream = tmpIn;
            mOutStream = tmpOut;

            Log.v(TAG, "BluetoothConnectedThread created !");
        }

        @Override
        public synchronized void start() {
            super.start();
            String cmd;
            if (mSwitchMode.isChecked()) {
                cmd = SWITCH_MODE_CMD + "1" + CMD_TERMINATOR;
            } else {
                cmd = SWITCH_MODE_CMD + "0" + CMD_TERMINATOR;
            }
            write(cmd.getBytes());
        }

        public void run() {
            Log.v(TAG, "BluetoothConnectedThread running...");
            mBuffer = new byte[1024];
            int numBytes;
            ArrayList<Byte> currentCmd = new ArrayList<>();

            while (true) {
                try {
                    numBytes = mInStream.read(mBuffer);
                } catch (IOException e) {
                    Log.v(TAG, "Input stream was disconnected", e);
                    break;
                }

                for (int i = 0; i < numBytes; i++) {
                    currentCmd.add(mBuffer[i]);
                }

                if (currentCmd.size() > 0) {
                    if (currentCmd.get(0) == Command.SHARP
                            && currentCmd.size() >= CommandSize.SHARP) {

                        Message readMsg = mmHandler.obtainMessage(
                                MessageConstants.MESSAGE_SHARP_CMD,
                                currentCmd.subList(0, CommandSize.SHARP).toArray());
                        readMsg.sendToTarget();
                        currentCmd.subList(0, CommandSize.SHARP).clear();
                    } else if (currentCmd.get(0) == Command.POSITION
                            && currentCmd.size() >= CommandSize.POSITION) {

                        Message readMsg = mmHandler.obtainMessage(
                                MessageConstants.MESSAGE_POSITION_CMD,
                                currentCmd.subList(0, CommandSize.POSITION).toArray());
                        readMsg.sendToTarget();
                        currentCmd.subList(0, CommandSize.POSITION).clear();
                    }
                }
            }
        }

        void write(byte[] bytes) {
            try {
                mOutStream.write(bytes);

                Message writtenMsg = mmHandler.obtainMessage(
                        MessageConstants.MESSAGE_WRITE, -1, -1, bytes);
                writtenMsg.sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);

                Message writeErrorMsg =
                        mmHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                bundle.putString(MessageConstants.TOAST_TEXT,
                        "Couldn't send data to the other device");
                writeErrorMsg.setData(bundle);
                mmHandler.sendMessage(writeErrorMsg);
            }
        }

        boolean isConnected() {
            return mSocket.isConnected();
        }

        void cancel() {
            try {
                Log.v(TAG, "Closing connect socket");
                mmHandler.obtainMessage(
                        MessageConstants.MESSAGE_DISCONNECTED, mSocket.getRemoteDevice().getName()
                ).sendToTarget();
                mSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }


}
